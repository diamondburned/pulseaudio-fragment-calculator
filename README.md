# pulseaudio-fragment-calculator

A PulseAudio fragment calculator, written in Bash. The manual instruction is here: [(super useful arch wiki link that barely anyone reads)](https://wiki.archlinux.org/index.php/PulseAudio/Troubleshooting#Setting_the_default_fragment_number_and_buffer_size_in_PulseAudio), but we're all lazy people. To reduce effort and to satisfy my lazyness, I wrote a script that does it for me.

## Usage

`./pulseaudio.sh`

## Important stuff

- Read what it asks carefully
- Carelessly running this script might break PulseAudio
- Before overriding the config, a backup will be made in `daemon.conf.old` and `default.conf.old`