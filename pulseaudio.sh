#!/bin/bash
# Finding out your audio device parameters (1/4)
SINKS=$(pactl list sinks) # prints sinks
SAVEIFS=$IFS; IFS=$'\n'
SINKSfound=( $(echo "$SINKS" | grep -F 'device.product.name' | awk -F '=' '{print $2}' | cut -d '"' -f 2) ) # grab buffer size from printed sinks
BUFFERsizefound=( $(echo "$SINKS" | grep -F device.buffering.buffer_size | awk -F '=' '{print $2}' | sed 's/[^0-9]*//g') ) # grab buffer size from printed sinks
FRAGMENTsizefound=( $(echo "$SINKS" | grep -F device.buffering.fragment_size | awk -F '=' '{print $2}' | sed 's/[^0-9]*//g') ) # grab fragment size from printed sinks


if [[ ${#BUFFERsizefound[@]} > 1 || ${#FRAGMENTsizefound[@]} > 1 || ${#SINKSfound[@]} > 1 ]]; then
	ENTRY=0
	echo "Here are available sinks found: "
	for e in ${SINKSfound[@]}; do
		echo "$((${ENTRY}+1))"': '"${SINKSfound[$ENTRY]}"
		echo 'Fragment size: '"${FRAGMENTsizefound[$ENTRY]}"' / Buffer size: '"${BUFFERsizefound[$ENTRY]}"
		ENTRY=$(($ENTRY+1))
	done
	echo ""
	read -p "Please pick a sink to find buffer/fragment size for: " CONT
	CHOICE=$(echo "$CONT" | sed 's/[^0-9]*//g')
	CHOICE=$(( $CHOICE -1 ))
	BUFFERsize=$( echo "${BUFFERsizefound[$CHOICE]}" | sed 's/[^0-9]*//g' ) 
	FRAGMENTsize=$( echo "${FRAGMENTsizefound[$CHOICE]}" | sed 's/[^0-9]*//g' ) 
	if [[ $BUFFERsize = "" || $FRAGMENTsize = "" ]]; then
		echo Apparently the buffer/fragment size you picked is empty, maybe something is wrong :S 
		exit 1
	fi
else
	BUFFERsize=${BUFFERsizefound[0]}
	FRAGMENTsize=${FRAGMENTsizefound[0]}
fi

IFS=$SAVEIFS

read -p "Implying that the sampling rate is 44100Hz, bit depth is 16 bits and it's a stereo output. Confirm? [y/n] (y) " CONT
if [[ $CONT = "n" ]]; then
	echo "Input your data, format: [sampling rate]@[bit depth]x[channels]"
	echo "/!NOTE!/ zeron confirmed that bit depth other than 16/32 does NOT work. Therefore, please pick either 16-bit or 32-bit."
	echo "Example: 44100@16x2 means 44100Hz, 16 bits and Stereo (dual-channel)"
	read -p "Input: " CONT
	BITDATA="$CONT"
	SAMPLINGRATE=$(echo "$BITDATA" | awk -F '@' '{print $1}' | sed 's/[^0-9]*//g')
	BITRATE=$(echo "$BITDATA" | awk -F '@' '{print $2}' | awk -F 'x' '{print $1}' | sed 's/[^0-9]*//g')
	CHANNELS=$(echo "$BITDATA" | awk -F '@' '{print $2}' | awk -F 'x' '{print $2}' | sed 's/[^0-9]*//g')
	read -p "Final confirmation, sampling rate is '"$SAMPLINGRATE"Hz', bit rate is '"$BITRATE" bits' and channel count is '"$CHANNELS" channel(s)'? [y/n] (y)"
	if [[ $SAMPLINGRATE = "" || $CHANNELS = "" || $BITRATE = "16" && $BITRATE = "32" && $BITRATE = "8" ]]; then
			echo Your argument was wrong! Please check again!
			exit 1
	else
		if [[ $CONT = "n" ]]; then
			echo Recheck everything and rerun the script!
			exit 1
		fi
	fi
else
	BITDATA="44100@16x2"
	SAMPLINGRATE=$(echo "$BITDATA" | awk -F '@' '{print $1}' | sed 's/[^0-9]*//g')
	BITRATE=$(echo "$BITDATA" | awk -F '@' '{print $2}' | awk -F 'x' '{print $1}' | sed 's/[^0-9]*//g')
	CHANNELS=$(echo "$BITDATA" | awk -F '@' '{print $2}' | awk -F 'x' '{print $2}' | sed 's/[^0-9]*//g')
fi 

BYTEORDER=$(lscpu | grep 'Byte Order' | awk -F '          ' '{print $2}')
if [[ $BYTEORDER = *"Big Endian"* ]]; then
	BITend="be"
else # BYTEORDER = "Little Endian"
	BITend="le"
fi

if [[ $BITRATE = "16" ]]; then
	BITRATEFORMAT='s16'"$BITend"
elif [[ $BITRATE = "32" ]]; then
	BITRATEFORMAT='float32'"$BITend"
elif [[ $BITRATE = "8" ]]; then
	echo "Oho, a bit-rate of 8? Someone likes living on the edge."
	BITRATEFORMAT="u8"
else
	BITRATEFORMAT='s16'"$BITend"
fi

# Calculate your fragment size in msecs and number of fragments (2/4)
SBC=$(( $SAMPLINGRATE * $BITRATE * $CHANNELS ))
BUFFERsize=$(echo "($BUFFERsize / $SBC * 1000)" | bc -l)
FRAGMENTsize=$(echo "($FRAGMENTsize / $SBC * 1000)" | bc -l)

FRAGMENTcount=$(echo "($BUFFERsize / $FRAGMENTsize)" | bc -l | awk '{print int($1+0.5)}')
FRAGMENTsizerounded=$(echo "$FRAGMENTsize" | awk '{print int($1+0.5)}')

if [[ $FRAGMENTsizerounded = "0" ]]; then
	FRAGMENTsizerounded=1
fi

# Modify PulseAudio's configuration file (3/4)
DAEMONconf=$(cat /etc/pulse/daemon.conf)
if [[ ! $DAEMONconf ]]; then
	echo An error has occured! No daemon.conf file found! Send help!
	exit 1
fi

TODELETE=( "default-sample-format" "default-sample-rate" "default-sample-channels" "default-fragments" "default-fragment-size-msec" "high-priority" "nice-level" "realtime-scheduling" "realtime-priority" "resample-method" "alternate-sample-rate" "Added by script")
for e in ${TODELETE[@]}; do
	DAEMONconf=$(echo "$DAEMONconf" | sed "/$e/d")
done

ADDTOCONF="

; Added by script
high-priority = yes
nice-level = -15

realtime-scheduling = yes
realtime-priority = 50

resample-method = speex-float-0

default-sample-format = ${BITRATEFORMAT}
default-sample-rate = ${SAMPLINGRATE}
alternate-sample-rate = ${SAMPLINGRATE}
default-sample-channels = ${CHANNELS}

default-fragments = ${FRAGMENTcount}
default-fragment-size-msec = ${FRAGMENTsizerounded}"

DAEMONconf="$DAEMONconf$ADDTOCONF"

echo "$DAEMONconf"
echo "^^^ That there is the script. Double-check and answer."
echo ""

read -p "Write the changes to /etc/pulse/daemon.conf? [y/n] (n)" CONT
if [[ $CONT = "y" ]]; then
	sudo mv -f /etc/pulse/daemon.conf /etc/pulse/daemon.conf.old
	sudo cp -f /etc/pulse/default.pa /etc/pulse/default.pa.old
	sudo sed -i 's/.*load-module module-udev-detect.*/load-module module-udev-detect tsched=0/' /etc/pulse/default.pa # Disabling timer-based scheduling (0/4)
	echo "$DAEMONconf" | sudo tee /etc/pulse/daemon.conf &> /dev/null
	echo "Restart PulseAudio daemon? [y/n] (y)"
	read -p "You might have to close all applications and relaunch them for audio to work again. " CONT
	if [[ $CONT = "n" ]]; then
		echo Bye then.
		exit 0
	else
		echo Restarting
		pulseaudio -k
	fi
else
	echo Bye then.
	exit 0
fi
